## Super Resolution LTS5

TODO

### Reference
* [1] [Photo-Realistic Single Image Super-Resolution Using a Generative Adversarial Network](https://arxiv.org/abs/1609.04802)
* [2] [https://github.com/tensorlayer/srgan]


### License

- For academic and non-commercial use only.
- For commercial use, please contact tensorlayer@gmail.com.
