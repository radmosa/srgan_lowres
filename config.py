from easydict import EasyDict as edict
import json

config = edict()
config.TRAIN = edict()

## Adam
config.TRAIN.batch_size = 16
config.TRAIN.lr_init = 1e-4
config.TRAIN.beta1 = 0.9

## initialize G
config.TRAIN.n_epoch_init = 1000
config.TRAIN.lr_decay_init = 0.1
config.TRAIN.decay_every_init = 400
    # config.TRAIN.decay_every_init = int(config.TRAIN.n_epoch_init / 2)

## adversarial learning (SRGAN)
config.TRAIN.n_epoch = 1000
config.TRAIN.lr_decay = 0.1
config.TRAIN.decay_every = 400
#config.TRAIN.decay_every = int(config.TRAIN.n_epoch / 2)

## train set location
#config.TRAIN.hr_img_path = 'data2017/DIV2K_train_HR/'
#config.TRAIN.lr_img_path = 'data2017/DIV2K_train_LR_bicubic/X4/'

'''config.TRAIN.hr_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_HR'
config.TRAIN.lr_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4'
config.TRAIN.real_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4_bi'''

config.TRAIN.hr_x4_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_HR'
config.TRAIN.hr_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_bicubic/x4'
config.TRAIN.lr_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_real/x4'
config.TRAIN.real_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_bicubic/x4'

'''config.TRAIN.hr_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4'
config.TRAIN.lr_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4'
config.TRAIN.real_img_path = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4'''
#config.TRAIN.hr_img_path = '/media/saeed/DATA/SuperResolutionData/image_net_train_hr/'
#config.TRAIN.lr_img_path = '/media/saeed/DATA/SuperResolutionData/image_net_train_lr/'
#config.TRAIN.hr_img_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/hr_images/'
#config.TRAIN.lr_img_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/lr_images_x2/'

config.VALID = edict()
## test set location
config.VALID.hr_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_bicubic/x4'
config.VALID.lr_img_path = '/media/saeed-lts5/Data-Saeed/SuperResolution/SR_dataset/LR_to_LR_sets/DIV2k_RealSR_800BIHD_real/x4'

#config.VALID.hr_img_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/hr_images/'
#config.VALID.lr_img_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/lr_images_x2/'

#config.VALID.hr_img_path = 'data2017/saeed_test/'
#config.VALID.lr_img_path = 'data2017/saeed_test_low/'
#config.VALID.hr_img_path = 'data2017/SR_testing_datasets/Urban100/'
#config.VALID.hr_img_path = '/media/saeed/DATA/SuperResolutionData/image_net_valid_hr/'
#config.VALID.lr_img_path = '/media/saeed/DATA/SuperResolutionData/image_net_valid_lr/'

## Segmentation settings
config.TRAIN.seg_label_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/labels/'
config.TRAIN.seg_mask_path = '/home/saeed-lts5/Documents/cocoapi-master/PythonAPI/labels_mask/'

def log_config(filename, cfg):
    with open(filename, 'w') as f:
        f.write("================================================\n")
        f.write(json.dumps(cfg, indent=4))
        f.write("\n================================================\n")
