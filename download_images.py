import numpy as np
import pandas as pd
from datetime import datetime
from functools import reduce
import operator 
import requests
import sys
import argparse
import csv
import Image
import urllib, cStringIO
import urllib2
import os
import time
data = pd.read_csv('/home/saeed/Downloads/fall11_urls.txt', sep="\t", header=None, error_bad_lines=False)
#print (data)
urls = data[1]
#print (urls)
urls = urls.sample(frac=1)
#print (urls)
#urls = urls.head(350000)
#print (urls)

downloaded = 0


for idd, ent in enumerate(urls):
    #print(idd)
    #print (ent)

    #Downdload and save image to ../images
    
    try:
        img_data = requests.get(ent, timeout=2).content
        if img_data is not b"":
            #print(str(downloaded) + ' images downloaded')

            with open('/media/saeed/DATA/image_net_sr/im_' + str(downloaded) +'.jpg', 'wb') as handler:
                try:
                    handler.write(img_data)
                    img = Image.open('/media/saeed/DATA/image_net_sr/im_' + str(downloaded) +'.jpg')
                except:
                    #time.sleep(1)
                    #print ('removinf file')
                    os.remove('/media/saeed/DATA/image_net_sr/im_' + str(downloaded) +'.jpg')
                    downloaded = downloaded - 1
            downloaded = downloaded + 1
    except:
        print('timeout')
    print ('#images: ' + str(downloaded))

    if downloaded == 350000:
        break

print('Finished')

