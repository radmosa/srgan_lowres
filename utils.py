import tensorflow as tf
import tensorlayer as tl
from tensorlayer.prepro import *

import cv2
# from config import config, log_config
#
# img_path = config.TRAIN.img_path

import scipy
import numpy as np

def get_imgs_fn(file_name, path):
    """ Input an image path and name, return an image array """
    # return scipy.misc.imread(path + file_name).astype(np.float)
    return scipy.misc.imread(path + file_name, mode='RGB')

def crop_sub_imgs_fn(x, is_random=True):
    x = crop(x, wrg=384, hrg=384, is_random=is_random)
    x = x / (255. / 2.)
    x = x - 1.
    return x

def crop_sub_vids_fn(x):
    x = crop(x, wrg=768, hrg=768, is_random=False)
    x = x / (255. / 2.)
    x = x - 1.
    return x

def downsample_vids_fn(x):
    # We obtained the LR images by downsampling the HR images using bicubic kernel with downsampling factor r = 4.
    x = imresize(x, size=[120, 160], interp='lanczos', mode=None)
    x = x / (255. / 2.)
    x = x - 1.
    return x

def downsample_fn(x):
    # We obtained the LR images by downsampling the HR images using bicubic kernel with downsampling factor r = 4.
    x = imresize(x, size=[96, 96], interp='bicubic', mode=None)
    x = x / (255. / 2.)
    x = x - 1.
    return x

def downsample_cv2_fn(x):
    # We obtained the LR images by downsampling the HR images using bicubic kernel with downsampling factor r = 4.
    x = imresize(x, size=[96, 96], interp='bicubic', mode=None)
    x = x / (255. / 2.)
    x = x - 1.
    return x

def crop_custom(x, w, h, is_random=0):
    if(len(x.shape) == 2):
        x = np.expand_dims(x,2)
        x = np.concatenate((x , x, x), axis=2)
    if(x.shape[0] <= w or x.shape[1] <= h):
        #print ('image smaller than expected, resizing ...\n')
        x = imresize(x, size=[w, h], interp='lanczos', mode=None)
    else:
        #try:
        x = crop_rand(x, wrg=w, hrg=h, crop_from=is_random)
        #except:
        #    print (x.shape)
        #    print (w,h)
    #x = x / (255. / 2.)
    #x = x - 1.
    return x[:,:,0:3]

def crop_custom_yuv(x, w, h, is_random=True):
    if(len(x.shape) == 2):
        x = np.expand_dims(x,2)
        x = np.concatenate((x , x, x), axis=2)
    if(x.shape[0] <= w or x.shape[1] <= h):
        #print ('image smaller than expected, resizing ...\n')
        x = imresize(x, size=[w, h], interp='lanczos', mode=None)
    else:
        #try:
        x = crop(x, wrg=w, hrg=h, is_random=is_random)
        #except:
        #    print (x.shape)
        #    print (w,h)
    im_final = np.zeros(x.shape)
    x = cv2.cvtColor(x, cv2.COLOR_RGB2YUV)
    y, u, v = cv2.split(x)
    y = y / (255. / 2.) - 1.0
    u = u / (255. / 2.) - 1.0
    v = v / (255. / 2.) - 1.0
    im_final[:,:,0] = y
    im_final[:,:,1] = u
    im_final[:,:,2] = v

    return im_final

def crop_custom_2(x, w, h, is_random=True):
    '''if(x.shape[0] <= w or x.shape[1] <= h):
        #print ('image smaller than expected, resizing ...\n')
        x = cv2.resize(x, (w, h), interpolation = cv2.INTER_NEAREST) #imresize(x, size=[w, h], interp='nearest', mode=None)
    else:
        #try:
        x = crop(x, wrg=w, hrg=h, is_random=is_random)'''

    x = cv2.resize(x, (w, h), interpolation = cv2.INTER_LANCZOS4) #imresize(x, size=[w, h], interp='nearest', mode=None)
    return x

def convert_to_three_channel(x):
    x = x[:,:,:3]
    return x

# crop
def crop_rand(x, wrg, hrg, crop_from=0, row_index=0, col_index=1):

    h, w = x.shape[row_index], x.shape[col_index]
    #print(x.shape)
    #print(crop_from)
    if (h < hrg) or (w < wrg):
        raise AssertionError("The size of cropping should smaller than or equal to the original image")

    h_offset = int(crop_from)
    w_offset = int(crop_from)
    #print(x[h_offset:hrg + h_offset, w_offset:wrg + w_offset].shape)
    # tl.logging.info(h_offset, w_offset, x[h_offset: hrg+h_offset ,w_offset: wrg+w_offset].shape)
    return x[h_offset:hrg + h_offset, w_offset:wrg + w_offset]
