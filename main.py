#! /usr/bin/python
# -*- coding: utf8 -*-

import os, time, pickle, random, time
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID" # so the IDs match nvidia-smi
os.environ["CUDA_VISIBLE_DEVICES"] = "0"

from datetime import datetime
import numpy as np
from time import localtime, strftime
import logging, scipy

import tensorflow as tf
import tensorlayer as tl

from skimage.measure import compare_ssim as ssim
from skimage import io
from skimage import img_as_float

import scipy

from model import SRGAN_g, SRGAN_g_lr, SRGAN_d, Vgg19_simple_api
from utils import *
from config import config, log_config

from PIL import Image
import cv2
from imresize import imresize



###====================== HYPER-PARAMETERS ===========================###
## Adam
batch_size = config.TRAIN.batch_size
lr_init = config.TRAIN.lr_init
beta1 = config.TRAIN.beta1
## initialize G
n_epoch_init = config.TRAIN.n_epoch_init
## adversarial learning (SRGAN)
n_epoch = config.TRAIN.n_epoch
lr_decay = config.TRAIN.lr_decay
decay_every = config.TRAIN.decay_every
decay_every_init = config.TRAIN.decay_every_init
lr_decay_init = config.TRAIN.lr_decay_init

ni = int(np.sqrt(batch_size))

train_only_generator = False
train_using_gan = True



def train():
    ## create folders to save result images and trained model
    current_time = datetime.now().strftime("%m%d%Y%H%M%S")
    save_dir_tensorboard = "./tensorboard/" + current_time
    save_dir_ginit = "samples/" + current_time + "/{}_ginit".format(tl.global_flag['mode'])
    save_dir_gan = "samples/" + current_time + "/{}_gan".format(tl.global_flag['mode'])
    #save_dir_gan = "samples/" + current_time + "/{}_gan".format(tl.global_flag['mode'])
    tl.files.exists_or_mkdir(save_dir_ginit)
    tl.files.exists_or_mkdir(save_dir_gan)
    tl.files.exists_or_mkdir(save_dir_tensorboard)
    checkpoint_dir = "checkpoint_output/" + current_time # checkpoint_resize_conv
    tl.files.exists_or_mkdir(checkpoint_dir)
    checkpoint_input_g = "/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/checkpoint_output/09302019183359/g_init_50.npz"
    checkpoint_input_d = "./donotexist.npz"
    ###====================== PRE-LOAD DATA ===========================###
    ## Read images from folder
    train_hr_img_list = sorted(tl.files.load_file_list(path=config.TRAIN.hr_img_path, regx='.*.png', printable=False))
    train_lr_img_list = sorted(tl.files.load_file_list(path=config.TRAIN.lr_img_path, regx='.*.png', printable=False))
    valid_hr_img_list = sorted(tl.files.load_file_list(path=config.VALID.hr_img_path, regx='.*.png', printable=False))
    valid_lr_img_list = sorted(tl.files.load_file_list(path=config.VALID.lr_img_path, regx='.*.png', printable=False))

    ## Read images from txt files
    #train_hr_img_list = [line.rstrip('\n') for line in open('train_hr_images.txt')]
    #config.TRAIN.hr_img_path = [line.rstrip('\n') for line in open('train_hr_paths.txt')]
    #train_lr_img_list = [line.rstrip('\n') for line in open('train_lr_images.txt')]
    #config.TRAIN.lr_img_path = [line.rstrip('\n') for line in open('train_lr_paths.txt')]

    ## If your machine have enough memory, please pre-load the whole train set.
    #train_hr_imgs = tl.vis.read_images(train_hr_img_list, path=config.TRAIN.hr_img_path, n_threads=32)
    #train_lr_imgs = tl.vis.read_images(train_lr_img_list, path=config.TRAIN.lr_img_path, n_threads=32)

    # for im in train_hr_imgs:
    #     print(im.shape)
    # valid_lr_imgs = tl.vis.read_images(valid_lr_img_list, path=config.VALID.lr_img_path, n_threads=32)
    # for im in valid_lr_imgs:
    #     print(im.shape)
    # valid_hr_imgs = tl.vis.read_images(valid_hr_img_list, path=config.VALID.hr_img_path, n_threads=32)
    # for im in valid_hr_imgs:
    #     print(im.shape)
    # exit()
    ###========================== DEFINE MODEL ============================###
    ## train inference
    t_target_image= tf.placeholder('float32', [batch_size, 64, 64, 3], name='t_target_image')
    t_real_pack= tf.placeholder('float32', [batch_size, 64, 64, 3], name='t_real_pack')
    t_image = tf.placeholder('float32', [batch_size, 256, 256, 3], name='t_image_input_to_SRGAN_generator')

    net_g = SRGAN_g_lr(t_image, is_train=True, reuse=False)
    net_d, logits_real = SRGAN_d(t_real_pack, is_train=True, reuse=False)
    print(net_g.outputs.shape)
    _, logits_fake = SRGAN_d(net_g.outputs, is_train=True, reuse=True)

    #net_g.print_params(False)
    #net_g.print_layers()

    ## vgg inference. 0, 1, 2, 3 BILINEAR NEAREST BICUBIC AREA
    '''t_target_image_224 = tf.image.resize_images(
        t_target_image, size=[224, 224], method=0,
        align_corners=False)  # resize_target_image_for_vgg # http://tensorlayer.readthedocs.io/en/latest/_modules/tensorlayer/layers.html#UpSampling2dLayer
    t_predict_image_224 = tf.image.resize_images(net_g.outputs, size=[224, 224], method=0, align_corners=False)  # resize_generate_image_for_vgg

    net_vgg, vgg_target_emb = Vgg19_simple_api((t_target_image_224 + 1) / 2, reuse=False)
    _, vgg_predict_emb = Vgg19_simple_api((t_predict_image_224 + 1) / 2, reuse=True)'''

    ## test inference
    net_g_test = SRGAN_g_lr(t_image, is_train=False, reuse=True)

    ssim_valid = tf.reduce_mean(tf.image.ssim(net_g_test.outputs, t_target_image, 1))
    #mse_valid = tf.losses.mean_squared_error(net_g_test.outputs, t_target_image)
    mse_valid = tf.losses.absolute_difference(net_g_test.outputs, t_target_image)

    # ###========================== DEFINE TRAIN OPS ==========================###
    d_loss1 = tl.cost.sigmoid_cross_entropy(logits_real, tf.ones_like(logits_real), name='d1')
    d_loss2 = tl.cost.sigmoid_cross_entropy(logits_fake, tf.zeros_like(logits_fake), name='d2')
    d_loss = d_loss1 + d_loss2

    g_gan_loss = 1e-3 * tl.cost.sigmoid_cross_entropy(logits_fake, tf.ones_like(logits_fake), name='g')
    mse_loss = tl.cost.absolute_difference_error(net_g.outputs, t_target_image, is_mean=True)
    '''vgg_loss = 2e-6 * tl.cost.mean_squared_error(vgg_predict_emb.outputs, vgg_target_emb.outputs, is_mean=True)'''

    g_loss = mse_loss + g_gan_loss #+ vgg_loss 

    g_vars = tl.layers.get_variables_with_name('SRGAN_g', True, True)
    d_vars = tl.layers.get_variables_with_name('SRGAN_d', True, True)

    ## Tensorboard summaries
    summary_1 = tf.summary.scalar("training/loss_mse", mse_loss)
    summary_2 = tf.summary.scalar("validation/loss_mse", mse_valid)
    summary_3 = tf.summary.scalar("validation/loss_ssim", ssim_valid)
    #summary_4 = tf.summary.scalar("training/loss_vgg", vgg_loss)
    summary_5 = tf.summary.scalar("training/loss_g_gan", g_gan_loss)
    summary_6 = tf.summary.scalar("training/loss_d_gan", d_loss)

    summary_op_train = summary_1#tf.summary.merge([summary_1, summary_4])
    summary_op_valid = tf.summary.merge([summary_2, summary_3])
    summary_op_train_g_gan = tf.summary.merge([summary_1, summary_5])
    summary_op_train_d_gan = tf.summary.merge([summary_6])

    with tf.variable_scope('learning_rate'):
        lr_v = tf.Variable(lr_init, trainable=False)
    ## Pretrain
    g_optim_init = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(mse_loss, var_list=g_vars)
    ## SRGAN
    g_optim = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(g_loss, var_list=g_vars)
    d_optim = tf.train.AdamOptimizer(lr_v, beta1=beta1).minimize(d_loss, var_list=d_vars)

    ###========================== RESTORE MODEL =============================###
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    tl.layers.initialize_global_variables(sess)
    #if tl.files.load_and_assign_npz(sess=sess, name=checkpoint_dir + '/g_{}.npz'.format(tl.global_flag['mode']), network=net_g) is False:
#        tl.files.load_and_assign_npz(sess=sess, name=checkpoint_dir + '/g_{}_init.npz'.format(tl.global_flag['mode']), network=net_g)
    tl.files.load_and_assign_npz(sess=sess, name=checkpoint_input_g, network=net_g)
    tl.files.load_and_assign_npz(sess=sess, name=checkpoint_input_d, network=net_d)

    ###============================= LOAD VGG ===============================###
    '''vgg19_npy_path = "vgg19.npy"
    if not os.path.isfile(vgg19_npy_path):
        print("Please download vgg19.npz from : https://github.com/machrisaa/tensorflow-vgg")
        exit()
    npz = np.load(vgg19_npy_path, encoding='latin1').item()

    params = []
    for val in sorted(npz.items()):
        W = np.asarray(val[1][0])
        b = np.asarray(val[1][1])
        print("  Loading %s: %s, %s" % (val[0], W.shape, b.shape))
        params.extend([W, b])
    tl.files.assign_params(sess, params, net_vgg)
    # net_vgg.print_params(False)
    # net_vgg.print_layers()
    '''
    ###============================= TRAINING ===============================###
    ## use first `batch_size` of train set to have a quick test during training
    train_hr_imgs = tl.vis.read_images(train_hr_img_list[0:1*batch_size], path=config.TRAIN.hr_img_path, n_threads=32)
    train_lr_imgs = tl.vis.read_images(train_lr_img_list[0:1*batch_size], path=config.TRAIN.lr_img_path, n_threads=32)
    sample_hr_imgs = train_hr_imgs[0:batch_size]
    sample_lr_imgs = train_lr_imgs[0:batch_size]

    # sample_imgs = tl.vis.read_images(train_hr_img_list[0:batch_size], path=config.TRAIN.hr_img_path, n_threads=32) # if no pre-load train set
    #sample_imgs_384 = tl.prepro.threading_data(sample_hr_imgs, fn=crop_sub_imgs_fn, is_random=False)
    #print('sample HR sub-image:', sample_imgs_384.shape, sample_imgs_384.min(), sample_imgs_384.max())
    #sample_imgs_96 = tl.prepro.threading_data(sample_imgs_384, fn=downsample_fn)
    #print('sample LR sub-image:', sample_imgs_96.shape, sample_imgs_96.min(), sample_imgs_96.max())

    sample_imgs_384 = tl.prepro.threading_data(sample_hr_imgs, fn=crop_custom, w=256, h=256, is_random=0)
    sample_imgs_96 = tl.prepro.threading_data(sample_lr_imgs, fn=crop_custom, w=64, h=64, is_random=0)

    #tl.vis.save_images(sample_imgs_96, [ni, ni], save_dir_ginit + '/_train_sample_96.png')
    tl.vis.save_images(sample_imgs_384, [ni, ni], save_dir_ginit + '/_train_sample_384.png')

    #this is for GAN
    tl.vis.save_images(sample_imgs_96, [ni, ni], save_dir_gan + '/_train_sample_96.png')
    tl.vis.save_images(sample_imgs_384, [ni, ni], save_dir_gan + '/_train_sample_384.png')

    ## Use 1st validation set to have a nicee test during training
    valid_hr_imgs = tl.vis.read_images(valid_hr_img_list[0:3*batch_size], path=config.VALID.hr_img_path, n_threads=32)
    valid_lr_imgs = tl.vis.read_images(valid_lr_img_list[0:3*batch_size], path=config.VALID.lr_img_path, n_threads=32)

    sample_valid_imgs = valid_hr_imgs[0:batch_size]
    sample_valid_lr_imgs = valid_lr_imgs[0:batch_size]
    sample_valid_imgs_384_1 = tl.prepro.threading_data(sample_valid_imgs, fn=crop_custom, w=256, h=256, is_random=0)
    sample_valid_imgs_96_1 = tl.prepro.threading_data(sample_valid_lr_imgs, fn=crop_custom, w=64, h=64, is_random=0)
    tl.vis.save_images(sample_valid_imgs_96_1, [ni, ni], save_dir_ginit + '/_valid_1_sample_96.png')
    tl.vis.save_images(sample_valid_imgs_384_1, [ni, ni], save_dir_ginit + '/_valid_1_sample_384.png')

    #this is for GAN
    tl.vis.save_images(sample_valid_imgs_96_1, [ni, ni], save_dir_gan + '/_valid_1_sample_96.png')
    tl.vis.save_images(sample_valid_imgs_384_1, [ni, ni], save_dir_gan + '/_valid_1_sample_384.png')

    ###========================= Tensorbox init ====================###
    writer = tf.summary.FileWriter(
        logdir=save_dir_tensorboard,
        flush_secs=1
	#graph=tf.get_default_graph()
    )

    ###========================= initialize G ====================###
    ## fixed learning rate
    sess.run(tf.assign(lr_v, lr_init))
    n_iter_total = 0
    print(" ** fixed learning rate: %f (for init G)" % lr_init)

    train_hr_img_list = train_hr_img_list[0:50000]
    train_lr_img_list = train_lr_img_list[0:50000]

    ## remove batches with less than batch size images
    train_hr_img_list = train_hr_img_list[0:(len(train_hr_img_list) - len(train_hr_img_list)%batch_size)]
    train_lr_img_list = train_lr_img_list[0:(len(train_lr_img_list) - len(train_lr_img_list)%batch_size)]

    for epoch in range(0, n_epoch_init + 1):

        if epoch != 0 and (epoch % decay_every_init == 0):
            new_lr_decay = lr_decay_init**(epoch // decay_every_init)
            sess.run(tf.assign(lr_v, lr_init * new_lr_decay))
            log = " ** new learning rate: %f (for GAN)" % (lr_init * new_lr_decay)
            print(log)

        if (train_only_generator == False):
            print("Training only generator is Off. Continuing to GAN ...")
            break
        ## -------------------------------Evaluation (of previous epoch)-------------------------------

        ## Evaluation on train set (first 4 images of training set)
        if (epoch % 1 == 0):
            #start_time = time.time()
            out = sess.run(net_g_test.outputs, {t_image: sample_imgs_384})  #; print('gen sub-image:', out.shape, out.min(), out.max())
            #print("took: %4.4fs" % (time.time() - start_time))
            print("[Evauation] save training images")
            tl.vis.save_images(out, [ni, ni], save_dir_ginit + '/train_%d.png' % epoch)
            #tl.vis.save_images(sample_imgs_96, [ni, ni], save_dir_ginit + '/train_lr_%d.png' % epoch)

        ## Evaluation on test set (first 4 images of test set) and add summary 
        if(epoch % 1 == 0):

            summary_str, out = sess.run([summary_op_valid, net_g_test.outputs], {t_image: sample_valid_imgs_384_1, t_target_image:sample_valid_imgs_96_1})
            #ssim = tf.reduce_mean(tf.image.ssim(tf.cast(tf.convert_to_tensor(out), np.float32), tf.cast(tf.convert_to_tensor(sample_valid_imgs_384_1), np.float32), 1))
            #mse_valid = tf.losses.mean_squared_error(tf.cast(tf.convert_to_tensor(out), np.float32), tf.cast(tf.convert_to_tensor(sample_valid_imgs_384_1), np.float32))
            tl.vis.save_images(out, [ni, ni], save_dir_ginit + '/valid_1_%d.png' % epoch)
            print("[Evauation] save validation images")
            #Add summary to tensorboard
            writer.add_summary(summary_str , global_step=n_iter_total)

        ## save model
        if (epoch != 0) and (epoch % 10 == 0):
            tl.files.save_npz(net_g.all_params, name=checkpoint_dir + '/g_init_'+ str(epoch)+'.npz', sess=sess)
            print("[Evauation] model saved as: " + checkpoint_dir + '/g_init_'+ str(epoch)+'.npz')

        ## update learning rate
        if epoch != 0 and (epoch % decay_every == 0):
            new_lr_decay = lr_decay**(epoch // decay_every)
            sess.run(tf.assign(lr_v, lr_init * new_lr_decay))
            log = " ** new learning rate: %f (for GAN)" % (lr_init * new_lr_decay)
            print(log)
        elif epoch == 0:
            sess.run(tf.assign(lr_v, lr_init))
            log = " ** init lr: %f  decay_every_init: %d, lr_decay: %f (for GAN)" % (lr_init, decay_every, lr_decay)
            print(log)

        ## -------------------------------Train for an epoch-------------------------------
        epoch_time = time.time()
        total_mse_loss, n_iter = 0, 0
        ## If your machine cannot load all images into memory, you should use
        ## this one to load batch of images while training.

        # shuffle images
        step_time = time.time()
        train_all_img_list = list(zip(train_hr_img_list, train_lr_img_list))
        random.shuffle(train_all_img_list)
        train_hr_img_list, train_lr_img_list = zip(*train_all_img_list)
        print("Shuffled time: %4.4fs" % (time.time() - step_time))

        #random.shuffle(train_hr_img_list)
        for idx in range(0, len(train_hr_img_list), batch_size):
            step_time = time.time()
            b_imgs_list = train_hr_img_list[idx : idx + batch_size]
            #b_imgs = tl.prepro.threading_data(b_imgs_list, fn=get_imgs_fn, path=config.TRAIN.hr_img_path)
            b_imgs = tl.vis.read_images(b_imgs_list, path=config.TRAIN.hr_img_path, n_threads=32, printable=False)

            b_imgs_lr_list = train_lr_img_list[idx : idx + batch_size]
            #b_imgs_lr = tl.prepro.threading_data(b_imgs_lr_list, fn=get_imgs_fn, path=config.TRAIN.lr_img_path)
            b_imgs_lr = tl.vis.read_images(b_imgs_lr_list, path=config.TRAIN.lr_img_path, n_threads=32, printable=False)

            #b_imgs_384 = tl.prepro.threading_data(b_imgs, fn=crop_sub_imgs_fn, is_random=True)
            #b_imgs_96 = tl.prepro.threading_data(b_imgs_384, fn=downsample_fn)
            lr_min = np.amin([b_imgs_lr[0].shape[0], b_imgs_lr[1].shape[0], b_imgs_lr[2].shape[0], b_imgs_lr[3].shape[0],
                              b_imgs_lr[0].shape[1], b_imgs_lr[1].shape[1], b_imgs_lr[2].shape[1], b_imgs_lr[3].shape[1]])
            rand_crop = int(np.random.uniform(0, lr_min - 64))
            b_imgs_384 = tl.prepro.threading_data(b_imgs, fn=crop_custom, w=256, h=256, is_random=rand_crop*4)
            b_imgs_96 = tl.prepro.threading_data(b_imgs_lr, fn=crop_custom, w=64, h=64, is_random=rand_crop)

        #train_hr_imgs = train_hr_imgs[0:100]
        #train_lr_imgs = train_lr_imgs[0:100]
        ## If your machine have enough memory, please pre-load the whole train set.
        # for idx in range(0, len(train_hr_imgs), batch_size):
        #     step_time = time.time()
        #     b_imgs_384 = tl.prepro.threading_data(train_hr_imgs[idx:idx + batch_size], fn=crop_custom, w=328, h=328, is_random=False)
        #     b_imgs_96 = tl.prepro.threading_data(train_lr_imgs[idx:idx + batch_size], fn=crop_custom, w=82, h=82, is_random=False)
        #     #b_imgs_96 = tl.prepro.threading_data(b_imgs_384, fn=downsample_fn)
            ## update G
            summary_str, errM, _ = sess.run([summary_op_train, mse_loss, g_optim_init], {t_image: b_imgs_384, t_target_image: b_imgs_96})
            #print("Epoch [%2d/%2d] %4d time: %4.4fs, mse: %.8f " % (epoch, n_epoch_init, n_iter, time.time() - step_time, errM))
            total_mse_loss += errM
            n_iter += 1
            n_iter_total += 1
            if n_iter_total%50 == 0:
                writer.add_summary(summary_str , global_step=n_iter_total)
                print("Epoch [%2d/%2d] %4d time: %4.4fs, mse: %.8f " % (epoch, n_epoch_init, n_iter_total, time.time() - step_time, errM))
        log = "[*] Epoch: [%2d/%2d] time: %4.4fs, mse: %.8f" % (epoch, n_epoch_init, time.time() - epoch_time, total_mse_loss / n_iter)
        print(log)

    ###========================= train GAN (SRGAN) =========================###
    for epoch in range(0, n_epoch + 1):

        if (train_using_gan == False):
            print("Using GAN is deactivated. Exiting loop ...")
            break

        ## Evaluation on train set (first 4 images of training set)
        if (epoch % 1 == 0):
            out = sess.run(net_g_test.outputs, {t_image: sample_imgs_384})
            print("[Evauation] save training images")
            tl.vis.save_images(out, [ni, ni], save_dir_gan + '/train_%d.png' % epoch)

        ## Evaluation on test set (first 4 images of test set) and add summary 
        if(epoch % 1 == 0):
            summary_str, out = sess.run([summary_op_valid, net_g_test.outputs], {t_image: sample_valid_imgs_384_1, t_target_image:sample_valid_imgs_96_1})
            tl.vis.save_images(out, [ni, ni], save_dir_gan + '/valid_1_%d.png' % epoch)
            print("[Evauation] save validation images")
            #Add summary to tensorboard
            writer.add_summary(summary_str , global_step=n_iter_total)

        ## save model
        if (epoch != 0) and (epoch % 10 == 0):
            tl.files.save_npz(net_g.all_params, name= checkpoint_dir + '/g_'+ str(epoch)+'.npz' , sess=sess)
            tl.files.save_npz(net_d.all_params, name= checkpoint_dir + '/d_'+ str(epoch)+'.npz', sess=sess)
            print("[Evauation] model saved as: " + checkpoint_dir + '/g_'+ str(epoch)+'.npz')
            print("[Evauation] model saved as: " + checkpoint_dir + '/d_'+ str(epoch)+'.npz')

        ## GAN --------------------------------------------------------
        ## update learning rate
        if epoch != 0 and (epoch % decay_every == 0):
            new_lr_decay = lr_decay**(epoch // decay_every)
            sess.run(tf.assign(lr_v, lr_init * new_lr_decay))
            log = " ** new learning rate: %f (for GAN)" % (lr_init * new_lr_decay)
            print(log)
        elif epoch == 0:
            sess.run(tf.assign(lr_v, lr_init))
            log = " ** init lr: %f  decay_every_init: %d, lr_decay: %f (for GAN)" % (lr_init, decay_every, lr_decay)
            print(log)

        epoch_time = time.time()
        total_d_loss, total_g_loss, n_iter = 0, 0, 0

        ## If your machine cannot load all images into memory, you should use
        ## this one to load batch of images while training.

        # shuffle images
        step_time = time.time()
        train_all_img_list = list(zip(train_hr_img_list, train_lr_img_list))
        random.shuffle(train_all_img_list)
        train_hr_img_list, train_lr_img_list = zip(*train_all_img_list)
        print("Shuffled time: %4.4fs" % (time.time() - step_time))

        for idx in range(0, len(train_hr_img_list), batch_size):
            step_time = time.time()
            b_imgs_list = train_hr_img_list[idx : idx + batch_size]
            b_imgs = tl.vis.read_images(b_imgs_list, path=config.TRAIN.hr_img_path, n_threads=32, printable=False)

            b_imgs_lr_list = train_lr_img_list[idx : idx + batch_size]
            b_imgs_lr = tl.vis.read_images(b_imgs_lr_list, path=config.TRAIN.lr_img_path, n_threads=32, printable=False)
            lr_min = np.amin([b_imgs_lr[0].shape[0], b_imgs_lr[1].shape[0], b_imgs_lr[2].shape[0], b_imgs_lr[3].shape[0],
                              b_imgs_lr[0].shape[1], b_imgs_lr[1].shape[1], b_imgs_lr[2].shape[1], b_imgs_lr[3].shape[1]])
            rand_crop = int(np.random.uniform(0, lr_min - 64))
            b_imgs_384 = tl.prepro.threading_data(b_imgs, fn=crop_custom, w=256, h=256, is_random=rand_crop*4)
            b_imgs_96 = tl.prepro.threading_data(b_imgs_lr, fn=crop_custom, w=64, h=64, is_random=rand_crop)

            rand_crop = int(np.random.uniform(0, lr_min - 64))
            b_imgs_real = tl.prepro.threading_data(b_imgs, fn=crop_custom, w=64, h=64, is_random=rand_crop*4)

            #tl.vis.save_images(b_imgs_real, [ni, ni], save_dir_gan + '/real_pack_%d.png' % idx)
        ## If your machine have enough memory, please pre-load the whole train set.
        # for idx in range(0, len(train_hr_imgs), batch_size):
        #     step_time = time.time()
        #     b_imgs_384 = tl.prepro.threading_data(train_hr_imgs[idx:idx + batch_size], fn=crop_sub_imgs_fn, is_random=True)
        #     b_imgs_96 = tl.prepro.threading_data(b_imgs_384, fn=downsample_fn)

            ## update D
            summary_str, errD, _ = sess.run([summary_op_train_d_gan, d_loss, d_optim], {t_image: b_imgs_384, t_target_image: b_imgs_96, t_real_pack: b_imgs_real})
            ## update G
            summary_str_2, errG, errM, errA, _ = sess.run([summary_op_train_g_gan, g_loss, mse_loss, g_gan_loss, g_optim], {t_image: b_imgs_384, t_target_image: b_imgs_96})

            total_d_loss += errD
            total_g_loss += errG
            n_iter += 1
            n_iter_total += 1
            if n_iter_total%50 == 0:
                writer.add_summary(summary_str + summary_str_2 , global_step=n_iter_total)
                print("Epoch [%2d/%2d] %4d time: %4.4fs, d_loss: %.8f g_loss: %.8f (mse: %.6f, adv: %.6f)" %
                      (epoch, n_epoch, n_iter_total, time.time() - step_time, errD, errG, errM, errA))

        log = "[*] Epoch: [%2d/%2d] time: %4.4fs, d_loss: %.8f g_loss: %.8f" % (epoch, n_epoch, time.time() - epoch_time, total_d_loss / n_iter,
                                                                              total_g_loss / n_iter)
        print(log)

    print('Finished.')

def evaluate():
    ## create folders to save result images
    save_dir = "samples/{}".format(tl.global_flag['mode'])
    tl.files.exists_or_mkdir(save_dir)
    checkpoint_dir = "checkpoint"

    ###====================== PRE-LOAD DATA ===========================###
    # train_hr_img_list = sorted(tl.files.load_file_list(path=config.TRAIN.hr_img_path, regx='.*.png', printable=False))
    # train_lr_img_list = sorted(tl.files.load_file_list(path=config.TRAIN.lr_img_path, regx='.*.png', printable=False))
    valid_hr_img_list = sorted(tl.files.load_file_list(path=config.VALID.hr_img_path, regx='.*.png', printable=False))
    valid_lr_img_list = sorted(tl.files.load_file_list(path=config.VALID.lr_img_path, regx='.*.png', printable=False))

    ## If your machine have enough memory, please pre-load the whole train set.
    # train_hr_imgs = tl.vis.read_images(train_hr_img_list, path=config.TRAIN.hr_img_path, n_threads=32)
    # for im in train_hr_imgs:
    #     print(im.shape)
    valid_lr_imgs = tl.vis.read_images(valid_lr_img_list, path=config.VALID.lr_img_path, n_threads=32)
    # for im in valid_lr_imgs:
    #     print(im.shape)
    valid_hr_imgs = tl.vis.read_images(valid_hr_img_list, path=config.VALID.hr_img_path, n_threads=32)
    # for im in valid_hr_imgs:
    #     print(im.shape)
    # exit()

    ###========================== DEFINE MODEL ============================###
    imid = 0  # 0: 企鹅  81: 蝴蝶 53: 鸟  64: 古堡
    valid_lr_img = valid_lr_imgs[imid]
    valid_hr_img = valid_hr_imgs[imid]
    # valid_lr_img = get_imgs_fn('test.png', 'data2017/')  # if you want to test your own image
    valid_lr_img = (valid_lr_img / 127.5) - 1  # rescale to ［－1, 1]
    valid_lr_img = valid_lr_img[:,:,:3]
    # print(valid_lr_img.min(), valid_lr_img.max())

    size = valid_lr_img.shape
    # t_image = tf.placeholder('float32', [None, size[0], size[1], size[2]], name='input_image') # the old version of TL need to specify the image size
    #t_image = tf.placeholder('float32', [32, None, None, 3], name='input_image')
    t_image = tf.placeholder('float32', [4, None, None, 3], name='input_image')

    net_g = SRGAN_g(t_image, is_train=False, reuse=False)

    ###========================== RESTORE G =============================###
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    tl.layers.initialize_global_variables(sess)
    tl.files.load_and_assign_npz(sess=sess, name=checkpoint_dir + '/g_srgan_2.npz', network=net_g)

    ###======================= EVALUATION =============================###
    start_time = time.time()
    #while 1 == 1:
    #out = sess.run(net_g.outputs, {t_image: [valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img]})

    # Warmup on a dummy image
    im_warmup = 0.2 * np.ones((96, 96, 3), dtype=np.uint8)
    for i in xrange(2):
        start_time = time.time()
        out = sess.run(net_g.outputs, {t_image: [im_warmup, im_warmup, im_warmup, im_warmup]})
        print("warm up took: %4.4fs" % (time.time() - start_time))

    start_time = time.time()        
    out = sess.run(net_g.outputs, {t_image: [valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img]})
    print("test 1 took: %4.4fs" % (time.time() - start_time))

    immm = [valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img]
    start_time = time.time()
    out = sess.run(net_g.outputs, {t_image: immm})
    print("test 2 took: %4.4fs" % (time.time() - start_time))

    start_time = time.time()
    #while 1 == 1:
    #out = sess.run(net_g.outputs, {t_image: [valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img]})

    out = sess.run(net_g.outputs, {t_image: [valid_lr_img, valid_lr_img, valid_lr_img, valid_lr_img]})
    print("test 3 took: %4.4fs" % (time.time() - start_time))

    print("LR size: %s /  generated HR size: %s" % (size, out.shape))  # LR size: (339, 510, 3) /  gen HR size: (1, 1356, 2040, 3)
    print("[*] save images")
    tl.vis.save_image(out[0], save_dir + '/valid_gen.png')
    tl.vis.save_images(out, [ni, ni], save_dir + '/valid_gen2.png')
    tl.vis.save_image(valid_lr_img, save_dir + '/valid_lr.png')
    tl.vis.save_image(valid_hr_img, save_dir + '/valid_hr.png')

    out_bicu = scipy.misc.imresize(valid_lr_img, [size[0] * 4, size[1] * 4], interp='bicubic', mode=None)
    tl.vis.save_image(out_bicu, save_dir + '/valid_bicubic.png')

#def mse(x, y):
#    return np.linalg.norm(x - y)
    
def mse(imageA, imageB):
	# the 'Mean Squared Error' between the two images is the
	# sum of the squared difference between the two images;
	# NOTE: the two images must have the same dimension
	err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
	err /= float(imageA.shape[0] * imageA.shape[1])
	
	# return the MSE, the lower the error, the more "similar"
	# the two images are
	return err
    
def evaluate_all():
    batch_size = 1
    ni = 1 
    ## create folders to save result images
    save_dir = "samples/{}".format(tl.global_flag['mode'])
    tl.files.exists_or_mkdir(save_dir)
    #checkpoint_dir = "checkpoint"
    checkpoint_dir = "./checkpoint_output/06152018152517"
    checkpoint_dir = "./checkpoint_output/07042018152459" #baseline trained with COCO
    #checkpoint_dir = "./checkpoint_output/07122018144335" #segmentation idea 
    checkpoint_dir = "./checkpoint_output/07242018180036"
    ###====================== PRE-LOAD DATA ===========================###
    valid_hr_img_list = sorted(tl.files.load_file_list(path=config.VALID.hr_img_path, regx='.*.png', printable=False))
    valid_lr_img_list = sorted(tl.files.load_file_list(path=config.VALID.lr_img_path, regx='.*.png', printable=False))

    valid_lr_imgs = tl.vis.read_images(valid_lr_img_list, path=config.VALID.lr_img_path, n_threads=32)
    valid_hr_imgs = tl.vis.read_images(valid_hr_img_list, path=config.VALID.hr_img_path, n_threads=32)

    ###========================== DEFINE MODEL ============================###
    imid = 0
    valid_lr_img = valid_lr_imgs[imid]
    valid_hr_img = valid_hr_imgs[imid]
    # valid_lr_img = get_imgs_fn('test.png', 'data2017/')  # if you want to test your own image
    #valid_lr_img = (valid_lr_img / 127.5) - 1  # rescale to ［－1, 1]
    valid_lr_img = valid_lr_img[:,:,:3]
    valid_hr_img = valid_hr_img[:,:,:3]
    #size = valid_lr_img.shape
    #t_image = tf.placeholder('float32', [1, size[0], size[1], size[2]], name='input_image')
    t_image = tf.placeholder('float32', [1, None, None, 3], name='input_image')
    t_target_image = tf.placeholder('float32', [1, None, None, 3], name='input_image_2')
    t_target_image_bicube = tf.placeholder('float32', [1, None, None, 3], name='input_image_3')
    #size = valid_hr_img.shape
    #print size
    net_g = SRGAN_g(t_image, is_train=False, reuse=False)

    #ssim_valid = tf.image.ssim((t_target_image_bicube)/2 + 0.5, t_target_image/2 + 0.5, 1)
    print net_g.outputs.shape
    print t_target_image.shape
    
    ss = 0 #shifting
    cc = 6#cropping
    ssim_valid = tf.image.ssim((net_g.outputs[:,cc:-ss-cc,cc:-ss-cc,:])/2 + 0.5, t_target_image[:,cc+ss:-cc,cc+ss:-cc,:]/2 + 0.5, 1)
    mse_valid = tf.losses.mean_squared_error(net_g.outputs/2 + 0.5, t_target_image/2 + 0.5)

    ###========================== RESTORE G =============================###
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    tl.layers.initialize_global_variables(sess)
    tl.files.load_and_assign_npz(sess=sess, name=checkpoint_dir + '/g_46.npz', network=net_g)

    ###======================= EVALUATION =============================###
    start_time = time.time()

    mse_all = 0
    mse_all_2 = 0
    mse_all_bi = 0
    ssim_all = 0
    ssim_all_2 = 0
    ssim_all_bi = 0
    
    for idx in range(0, len(valid_hr_imgs), batch_size):

        ## High resolution image
        valid_hr_img = valid_hr_imgs[idx]
        valid_hr_img = valid_hr_img[:,:,:3]

        ## Low resolution image
        valid_lr_img_org = valid_lr_imgs[idx]
        valid_lr_img_org = valid_lr_img_org[:,:,:3]

        #just for test
        #if idx == 0:
        #    tl.vis.save_images(np.reshape(valid_lr_img,(1,valid_hr_img.shape[0],valid_hr_img.shape[1],valid_hr_img.shape[2])), [ni, ni], './low_org.png')
        #    valid_lr_img_2 = scipy.misc.imresize(valid_hr_img, (valid_hr_img.shape[0]/4,valid_hr_img.shape[1]/4), interp='lanczos')
        #    tl.vis.save_images(np.reshape(valid_lr_img_2,(1,valid_lr_img.shape[0],valid_lr_img.shape[1],valid_lr_img.shape[2])), [ni, ni], './low_lanczos.png')
        #    valid_lr_img_3 = cv2.resize(valid_hr_img, (0,0), fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC) 
        #    tl.vis.save_images(np.reshape(valid_lr_img_3,(1,valid_lr_img.shape[0],valid_lr_img.shape[1],valid_lr_img.shape[2])), [ni, ni], './low_cv2.png')
        #---------------------------------------
        # start_time = time.time()
        #valid_lr_img = cv2.resize(valid_hr_img, (valid_hr_img.shape[1]/4,valid_hr_img.shape[0]/4), interpolation = cv2.INTER_LANCZOS)
        #valid_lr_img = valid_hr_img.resize((valid_hr_img.shape[1]/4,valid_hr_img.shape[0]/4), Image.ANTIALIAS) 
        #print("resize cv2 took: %4.4fs" % (time.time() - start_time))

        #start_time = time.time()
        #valid_lr_img = imresize(valid_hr_img, 0.25)
        #print("resize custom took: %4.4fs" % (time.time() - start_time))

        #valid_lr_img = scipy.misc.imresize(valid_hr_img, (valid_hr_img.shape[0]/4,valid_hr_img.shape[1]/4), interp='lanczos')
        #valid_lr_img = cv2.resize(valid_hr_img, (0,0), fx=0.25, fy=0.25, interpolation = cv2.INTER_CUBIC) 

        ## Bicubic High resolution image
        valid_hr_img_bicube = scipy.misc.imresize(valid_lr_img_org, (valid_hr_img.shape[0],valid_hr_img.shape[1]), interp='bicubic')


        valid_lr_img_org = (valid_lr_img_org / 127.5) - 1
        valid_hr_img = (valid_hr_img / 127.5) - 1 
        valid_hr_img_bicube = (valid_hr_img_bicube / 127.5) - 1
        #print valid_lr_img_org.shape
        #print valid_hr_img.shape
        out, mse_, ssim_= sess.run([net_g.outputs, mse_valid, ssim_valid], {t_image: [valid_lr_img_org], t_target_image: [valid_hr_img], t_target_image_bicube: [valid_hr_img_bicube]})

        ## Save first image
        if idx == 0:
            tl.vis.save_images(np.reshape(valid_lr_img_org,(1,valid_lr_img_org.shape[0],valid_lr_img_org.shape[1],valid_lr_img_org.shape[2])), [ni, ni], './im_low_res_org.png')        
            #tl.vis.save_images(np.reshape(valid_lr_img_2,(1,valid_lr_img_2.shape[0],valid_lr_img_2.shape[1],valid_lr_img_2.shape[2])), [ni, ni], './im_low_res_org.png')        
            tl.vis.save_images(np.reshape(valid_hr_img,(1,valid_hr_img.shape[0],valid_hr_img.shape[1],valid_hr_img.shape[2])), [ni, ni], './im_high_res_org.png')
            tl.vis.save_images(np.reshape(valid_hr_img_bicube,(1,valid_hr_img.shape[0],valid_hr_img.shape[1],valid_hr_img.shape[2])), [ni, ni], './im_high_res_bi.png')
            tl.vis.save_images(out, [ni, ni], './im_high_res_sr.png')
            print('sample HR (org):', valid_hr_img.shape, valid_hr_img.min(), valid_hr_img.max())
            print('sample HR (bi):', valid_hr_img_bicube.shape, valid_hr_img_bicube.min(), valid_hr_img_bicube.max())
            out_tmp = np.reshape(out,(valid_hr_img.shape[0],valid_hr_img.shape[1],valid_hr_img.shape[2]))
            print('sample SR:', out_tmp.shape, out_tmp.min(), out_tmp.max())

        valid_sr_img = np.reshape(out,(valid_hr_img.shape[0],valid_hr_img.shape[1],valid_hr_img.shape[2]))
        
        ## Cropping margin
        cc = 4
        valid_hr_img = valid_hr_img[cc:-cc,cc:-cc]
        valid_hr_img_bicube = valid_hr_img_bicube[cc:-cc,cc:-cc]
        valid_sr_img = valid_sr_img[cc:-cc,cc:-cc]
        
        ssim_2 = ssim(img_as_float(valid_hr_img/2+0.5), img_as_float(valid_sr_img/2+0.5), data_range=1, multichannel=True)
        ssim_2_bi = ssim(img_as_float(valid_hr_img/2+0.5), img_as_float(valid_hr_img_bicube/2+0.5), data_range=1, multichannel=True)
        mse_2 = mse(img_as_float(valid_hr_img/2+0.5), img_as_float(valid_sr_img/2+0.5))
        mse_2_bi = mse(img_as_float(valid_hr_img/2+0.5), img_as_float(valid_hr_img_bicube/2+0.5))

        ssim_all = ssim_all + ssim_[0]        
        ssim_all_2 = ssim_all_2 + ssim_2
        ssim_all_bi = ssim_all_bi + ssim_2_bi
        
        mse_all = mse_all + mse_
        mse_all_2 = mse_all_2 + mse_2
        mse_all_bi = mse_all_bi + mse_2_bi
        
        #tl.vis.save_images(out, [ni, ni], save_dir + '/valid_all_'+ str(idx)+'.png')

        #print (str(idx) + ' (tf.sr): mse: ' + mse_.astype('str') + ', ssim: ' + ssim_[0].astype('str'))
        print (str(idx) + ' (ski.sr): mse: ' + mse_2.astype('str') + ', ssim: ' + ssim_2.astype('str'))
        print (str(idx) + ' (ski.bi): mse: ' + mse_2_bi.astype('str') + ', ssim: ' + ssim_2_bi.astype('str'))
        
        
    #print ('Final: (tf.sr): mse_total: ' + (mse_all/len(valid_hr_imgs)).astype('str') + ', ssim_total: ' + (ssim_all /len(valid_hr_imgs)).astype('str'))
    print ('Final: (ski.sr): mse_total: ' + (mse_all_2/len(valid_hr_imgs)).astype('str') + ', ssim_total: ' + (ssim_all_2 /len(valid_hr_imgs)).astype('str'))
    print ('Final: (ski.bi): mse_total: ' + (mse_all_bi/len(valid_hr_imgs)).astype('str') + ', ssim_total: ' + (ssim_all_bi /len(valid_hr_imgs)).astype('str'))
    
    print("took(per image): %4.4fs" % ((time.time() - start_time)/(len(valid_hr_imgs)/batch_size)))

import re

def tryint(s):
    try:
        return int(s)
    except:
        return s

def alphanum_key(s):
    """ Turn a string into a list of string and number chunks.
        "z23a" -> ["z", 23, "a"]
    """
    return [ tryint(c) for c in re.split('([0-9]+)', s) ]

def sort_nicely(l):
    """ Sort the given list in the way that humans expect.
    """
    l.sort(key=alphanum_key)

def evaluate_video():

    video_dir = 'data2017/DIV2K_valid_HR/'
    video_dir = 'data2017/vidd'
    video_dir = '/media/saeed/DATA/SuperResolutionData/video_1/video_lr/'
    video_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_video/srgan_video-master/video_input/video_lr_1/'
    video_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/data2017/DIV2K_train_HR/'
    #video_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_HR/'

    batch_size = 1
    ni = 1 

    ## create folders to save output video frames
    save_dir = "video_out_3/"
    save_dir = "size_test/"
    save_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/data2017/DIV2K_train_LR/LR_09242019140052/'
    save_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/Data/RealSR_LR_x4_09242019140052/'
    save_dir = '/home/saeed-lts5/Documents/Super_Resolution/srgan_lowres/srgan_lowres-master/data2017/DIV2K_train_LR/LR_09302019200249/'
    tl.files.exists_or_mkdir(save_dir)

    checkpoint_dir = "checkpoint_output/09302019200249"

    ###====================== PRE-LOAD DATA ===========================###

    number_of_frames = 800 #len(video_imgs)

    video_img_list = sorted(tl.files.load_file_list(path=video_dir, regx='.*.png', printable=False))
    video_img_list = sorted(video_img_list,key= lambda f: int(''.join(filter(str.isdigit, f))) )

    video_imgs = tl.vis.read_images(video_img_list[0:number_of_frames], path=video_dir, n_threads=32)
    print (video_imgs[0].shape)
    image_test = []
    height = 0
    width = 0
    for idx in range(0, number_of_frames):#len(video_imgs)):
        #video_imgs[idx] = tl.prepro.threading_data([video_imgs[idx]], fn=crop_sub_vids_fn)[0]
        #video_imgs[idx] = np.reshape(video_imgs[idx], (768,768,3))
        #print video_imgs[idx].shape
        #video_imgs[idx] = (tl.prepro.imresize(video_imgs[idx], (3840/4,2160/4),interp='lanczos')[:,:,:3]/ 127.5) - 1
        #video_imgs[idx] = (tl.prepro.imresize(video_imgs[idx], (1280,720),interp='lanczos')[:,:,:3]/ 127.5) - 1
        video_imgs[idx] = (video_imgs[idx] /127.5) - 1

        #video_imgs[idx] = tl.prepro.threading_data([video_imgs[idx]], fn=downsample_vids_fn)[0]
        #im_to_save = np.reshape(video_imgs[idx],(1,video_imgs[idx].shape[0],video_imgs[idx].shape[1],video_imgs[idx].shape[2]))
        #tl.vis.save_images(im_to_save, [ni, ni], save_dir + 'frame_' + str(idx) + '_lr.png')

        if idx == 0:
            image_test = video_imgs[idx]
            height, width, layers = image_test.shape
            print(height)
            print(width)
            
    ###========================== Video Output ============================###
    #video = cv2.VideoWriter('/home/saeed/Documents/Super_Resolution/srgan/srgan-master/video_1.mp4', -1, 1, (width,height))
    fourcc = cv2.VideoWriter_fourcc(*"MJPG")
    video = cv2.VideoWriter('/home/saeed/Documents/Super_Resolution/srgan/srgan-master/size_test/video_1.avi', fourcc, 30.0, (width * 2, height * 2)) 
    ###========================== DEFINE MODEL ============================###
    t_image = tf.placeholder('float32', [batch_size, None, None, 3], name='input_image')
    net_g = SRGAN_g_lr(t_image, is_train=False, reuse=False)

    ###========================== Create bicubic video ============================###
    '''video_dir_bi = 'video_input/video_hr_1/'
    video_img_bi_list = sorted(tl.files.load_file_list(path=video_dir_bi, regx='.*.png', printable=False))
    video_img_bi_list = sorted(video_img_list,key= lambda f: int(''.join(filter(str.isdigit, f))) )
    video_imgs_bi = tl.vis.read_images(video_img_bi_list[0:number_of_frames], path=video_dir_bi, n_threads=32)
    image_test_bi = video_imgs_bi[0]
    height, width, layers = image_test_bi.shape
    video_bi = cv2.VideoWriter('/home/saeed/Documents/Super_Resolution/srgan/srgan-master/video_1_hr.avi', fourcc, 30.0, (width, height)) 
    
    for idx in range(0, number_of_frames):
        print (video_imgs_bi[idx].shape)
        video_bi.write(np.uint8( cv2.cvtColor(video_imgs_bi[idx], cv2.COLOR_BGR2RGB) ))
    cv2.destroyAllWindows()
    video.release()
    print('bicubic video done!')
    return'''
    ###========================== RESTORE G =============================###
    sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=False))
    tl.layers.initialize_global_variables(sess)
    tl.files.load_and_assign_npz(sess=sess, name=checkpoint_dir + '/g_400.npz', network=net_g) 

    ###======================= EVALUATION =============================###

    ## Warmup on a dummy image
    im_warmup = 0.2 * np.ones((192, 192, 3), dtype=np.float32)
    for i in xrange(10):
        start_time = time.time()
        out = sess.run(net_g.outputs, {t_image: [image_test]})
        #out = sess.run(net_g.outputs, {t_image: [image_test]})
        print("warm up took: %4.4fs" % (time.time() - start_time))   

    ## GO
    start_time_all = time.time()
    counter = 0
    for idx in range(0, len(video_imgs), batch_size):
        start_time = time.time()
        counter = counter + 1
        #video_img = video_imgs[idx]
        #video_img = video_img[:,:,:3]
        #valid_lr_img = scipy.misc.imresize(valid_hr_img, (valid_hr_img.shape[0]/4,valid_hr_img.shape[1]/4), interp='bicubic')
        #valid_lr_img = np.array(Image.fromarray(valid_hr_img).resize((valid_hr_img.shape[1]/4,valid_hr_img.shape[0]/4), Image.ANTIALIAS))
        #valid_hr_img_bicube = scipy.misc.imresize(valid_lr_img, (valid_hr_img.shape[0],valid_hr_img.shape[1]), interp='bicubic')


        #video_img = (video_img / 127.5) - 1
        #out = sess.run([net_g.outputs], {t_image: [video_imgs[0]]})
        out = sess.run([net_g.outputs], {t_image: [video_imgs[idx]]})
        #print(np.array(out).shape) 
        out = np.array(out) * 0.5 + 0.5#* 127.5 + 127.5
        #imagee = np.reshape(out[0],(out[0].shape[1],out[0].shape[2],out[0].shape[3]))

        #video.write(np.uint8( cv2.cvtColor(imagee, cv2.COLOR_BGR2RGB) ))
        #tl.vis.save_images(np.array([cv2.cvtColor(imagee, cv2.COLOR_BGR2RGB)]), [ni, ni], save_dir + video_img_list[idx])
        tl.vis.save_images(out[0], [ni, ni], save_dir + video_img_list[idx])
        print ('frame %d, duration: %4.4f' % (idx, (time.time() - start_time)/batch_size) )

    print("took(per image): %4.4fs" % ((time.time() - start_time_all)/(len(video_imgs)/batch_size)))
    print("FPS: %4.4fs" % (float(counter)/(time.time() - start_time_all)) )
    cv2.destroyAllWindows()
    video.release()

def debugg_block():
    
    img_bi = img_as_float(io.imread('high_res_bi.png'))
    img_sr = img_as_float(io.imread('high_res_sr.png'))
    img_hr = img_as_float(io.imread('high_res.png'))
    
    ssim_ = ssim(img_hr[:,:,2], img_sr[:,:,2], data_range=1, multichannel=True)  
    print ('normal cropping:') 
    print (ssim_)
    ssim_ = ssim(img_hr[:,:,2], img_bi[:,:,2], data_range=1, multichannel=True)   
    print (ssim_)

    mse_1 = mse(img_hr[:,:,2], img_sr[:,:,2])
    mse_2 = mse(img_hr[:,:,2], img_bi[:,:,2])

    print('mseee')
    print (mse_1)
    print (mse_2)
    #print ('new cropping:') 
    
'''
    for i in range(0, 4):
        j = 0
        img_bi_ = img_bi[j+0+i:img_hr.shape[0]-j,j+0+i:img_hr.shape[1]-j]
        img_sr_ = img_sr[j+0:img_hr.shape[0]-i-j,j+0:img_hr.shape[1]-i-j]
        img_hr_ = img_hr[j+0+i:img_hr.shape[0]-j,j+0+i:img_hr.shape[1]-j]
        #io.imsave('high_res_hr_.png', img_hr_)
        #io.imsave('high_res_bi_.png', img_bi_)
        #io.imsave('high_res_sr_.png', img_sr_)
    
        ssim_ = ssim(img_hr_, img_sr_, data_range=1, multichannel=True)  
        print (i) 
        print (ssim_)
        ssim_ = ssim(img_hr_, img_bi_, data_range=1, multichannel=True)   
        print (ssim_)
      '''  
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()

    parser.add_argument('--mode', type=str, default='srgan', help='srgan, evaluate')

    args = parser.parse_args()

    tl.global_flag['mode'] = args.mode

    if tl.global_flag['mode'] == 'srgan':
        train()
    elif tl.global_flag['mode'] == 'evaluate':
        evaluate()
    elif tl.global_flag['mode'] == 'evaluate_all':
        evaluate_all()
    elif tl.global_flag['mode'] == 'evaluate_video':
        evaluate_video()
    elif tl.global_flag['mode'] == 'debugg':
        debugg_block()
    else:
        raise Exception("Unknow --mode")
